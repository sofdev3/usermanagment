/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.usermanagement;

/**
 *
 * @author acer
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService.addUser("User2", "password");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("User3", "password"));
        System.out.println(UserService.getUsers());

        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("1234");
        UserService.UpdateUser(3, user);
        System.out.println(UserService.getUsers());

        UserService.delUser(user);
        System.out.println(UserService.getUsers());

        System.out.println(UserService.login("admin", "password"));
    }
}
